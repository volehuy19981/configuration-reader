#include "CRConfigurationReader.hpp"

#include <iostream>
#include <fstream>
#include <sstream>
#include <map>
#include "CFStructureTokenConfigurationFile.hpp"
#include <vector>
#include <algorithm>

void CRConfigurationReader::Read()
{
	std::fstream inFile(_configFile.GetConfigFilePath());
	int screenWidth = 0;
	int screenHeight = 0;
	bool isFullScreen = false;
	std::string gameTitle = "Unknown";
	std::map<std::string, std::string> configValues;
	std::vector<std::string> configTokens;
	do
	{
		if (!inFile.is_open())
		{
			// load config file fail
			std::cout << "Load config.txt fail" << std::endl;
			break;
		}

		configTokens.emplace_back(TOKEN_APP_TITLE);
		configTokens.emplace_back(TOKEN_APP_WIDTH);
		configTokens.emplace_back(TOKEN_APP_HEIGHT);
		configTokens.emplace_back(TOKEN_APP_FULLSCREEN);

		configValues[TOKEN_APP_TITLE] = "";
		configValues[TOKEN_APP_WIDTH] = "";
		configValues[TOKEN_APP_HEIGHT] = "";
		configValues[TOKEN_APP_FULLSCREEN] = "";

		std::string line;
		int tokenIndex = 0;
		while (std::getline(inFile, line))
		{
			bool nextLine = false;
			if (tokenIndex < configTokens.size())
			{
				std::istringstream is_line{ line };
				std::string key;
				if (std::getline(is_line, key, '='))
				{
					for (; tokenIndex < configTokens.size() && !nextLine; tokenIndex++)
					{
						std::string value;
						if (key[0] == '#')
						{
							nextLine = true;
							continue;
						}
						else if (key != configTokens[tokenIndex])
						{
							nextLine = true;
							continue;
						}

						if (std::getline(is_line, value))
						{
							configValues[key] = value;
							tokenIndex++;
							break;
						}
					}
				}
			}
		}

		auto iteratorFound = std::find_if(configValues.begin(), configValues.end(), [](const auto& pair)
		{
			return pair.second == "";
		});

		if (iteratorFound != configValues.end())
		{
			// found empty config property
			printf("Empty config property: %s\n", iteratorFound->first.c_str());
			break;
		}

		// set game title
		_configFile.SetGameTittle(configValues[TOKEN_APP_TITLE]);

		// set game width
		_configFile.SetScreenWidth(std::stoi(configValues[TOKEN_APP_WIDTH]));

		// set game height
		_configFile.SetScreenHeight(std::stoi(configValues[TOKEN_APP_HEIGHT]));

		// set game full screen
		bool isFullScreen = false;
		std::istringstream is;
		std::transform(
			configValues[TOKEN_APP_FULLSCREEN].begin(), configValues[TOKEN_APP_FULLSCREEN].end(),
			configValues[TOKEN_APP_FULLSCREEN].begin(), ::tolower);
		is >> std::boolalpha >> isFullScreen;
		_configFile.SetFullScreen(isFullScreen);

		// close stream
		inFile.close();
	} while (false);
}

CFConfigurationFile CRConfigurationReader::GetConfig()
{
	return _configFile;
}
