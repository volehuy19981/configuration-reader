#pragma once

const char* TOKEN_APP_TITLE = "AppTitle";
const char* TOKEN_APP_WIDTH = "WidthScreen";
const char* TOKEN_APP_HEIGHT = "HeightScreen";
const char* TOKEN_APP_FULLSCREEN = "FullScreen";