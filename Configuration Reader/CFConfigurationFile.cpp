#include "CFConfigurationFile.hpp"

CFConfigurationFile::CFConfigurationFile() :
	_configFilePath("config.txt")
{
}

#pragma region GET
int CFConfigurationFile::GetScreenWidth()
{
	return _screenWidth;
}


int CFConfigurationFile::GetScreenHeight()
{
	return _screenHeight;
}

std::string CFConfigurationFile::GetGameTitle()
{
	return _gameTitle;
}

std::string CFConfigurationFile::GetConfigFilePath()
{
	return _configFilePath;
}

bool CFConfigurationFile::IsFullScreen()
{
	return _isFullScreen;
}
#pragma endregion

#pragma region SET
void CFConfigurationFile::SetGameTittle(std::string gameTitle)
{
	_gameTitle = gameTitle;
}

void CFConfigurationFile::SetScreenWidth(int screenWidth)
{
	_screenWidth = screenWidth;
}

void CFConfigurationFile::SetScreenHeight(int screenHeight)
{
	_screenHeight = screenHeight;
}

void CFConfigurationFile::SetFullScreen(bool fullScreen)
{
	_isFullScreen = fullScreen;
}
#pragma endregion